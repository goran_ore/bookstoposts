﻿using BookToPosts.Models.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookToPosts.Models.Models
{
	public class Paragraph : IParagraph
	{
		public string FullText { get; set; }

		public int Id { get; set; }

		public int OrdinalNumber { get; set; }

		public string SeparatorMark { get; set; }

		public int Page { get; set; }

		public IEnumerable<IPicture> Pictures { get; set; }
		public string Title { get; set; }
	}
}
