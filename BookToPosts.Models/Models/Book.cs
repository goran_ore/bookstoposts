﻿using BookToPosts.Models.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookToPosts.Models.Common.Configurations;
using BookToPosts.Helpers.Converters;

namespace BookToPosts.Models.Models
{
	public class Book : IBook
	{
		public Book()
		{

		}
		public IEnumerable<IParagraph> AfterParagraphs { get; set; }

		public IEnumerable<IAuthor> Authors { get; set; }

		public IEnumerable<IParagraph> BeforeParagraphs { get; set; }

		public IParsingConfiguration BookParsingConfiguration { get; set; }

		public IEnumerable<IChapter> Chapters { get; set; }

		public int Id { get; set; }

		public string Title { get; set; }

		public void AddChapterToBook(IChapter chapter)
		{
			if (Chapters == null)
			{
				Chapters = new List<IChapter>();
			}
			List<IChapter> currentChapters = (List<IChapter>)Chapters;			
			NumeralsConverter convertNumerals = new NumeralsConverter();
			

			//Could be in first level chapter, or subchapter
			//Could be the first one in the list
			if (currentChapters != null && currentChapters.Count() == 0)
			{
				currentChapters.Add(chapter);
			}
			else
			{
				//Could be in the first level, find last chapter in the list
				IChapter beforeChapter = currentChapters.OrderByDescending(c => c.ArabicChapterMark).FirstOrDefault();
				//They are the same level if number of splitted marks are the same, same numeral style
				if (chapter.SplittedOriginalChapterMark.Count() == beforeChapter.SplittedOriginalChapterMark.Count() && 
					(convertNumerals.IsRoman(chapter.SplittedOriginalChapterMark[0]) == convertNumerals.IsRoman(beforeChapter.SplittedOriginalChapterMark[0])))
				{
					currentChapters.Add(chapter);
				}
				else
				{
					//It must be subchapter, add to last one
					beforeChapter.AddSubchapterChapterToBook(chapter);
				}
			}
		}

		public string FullText()
		{
			throw new NotImplementedException();
		}
	}
}
