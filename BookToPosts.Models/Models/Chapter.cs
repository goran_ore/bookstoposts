﻿using BookToPosts.Helpers.Converters;
using BookToPosts.Models.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookToPosts.Models.Models
{
	public class Chapter : IChapter
	{
		private string _originalChapterMark;

		public IEnumerable<IParagraph> AfterParagraphs { get; set; }

		public IEnumerable<IParagraph> BeforeParagraphs { get; set; }

		public string OriginalChapterMark
		{
			get
			{
				return _originalChapterMark;
			}
			set
			{
				_originalChapterMark = value;
				SetArabicChapterMark();
			}
		}
		public string ArabicChapterMark { get; set; }

		public int Id { get; set; }

		public int OrdinalNumber { get; set; }

		public string Title { get; set; }

		public IEnumerable<IChapter> SubChapters { get; set; }

		/// <summary>
		/// Determine which level of title in hierarchy 
		/// TODO: Add support for other numerals separator (like ,;: ...)
		/// </summary>
		public string[] SplittedOriginalChapterMark
		{
			get
			{
				return OriginalChapterMark.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
			}
		}

		public string FullText()
		{
			throw new NotImplementedException();
		}

		public void AddSubchapterChapterToBook(IChapter chapter)
		{
			if (SubChapters == null)
			{
				SubChapters = new List<IChapter>();
			}
			List<IChapter> currentChapters = (List<IChapter>)SubChapters;
			NumeralsConverter convertNumerals = new NumeralsConverter();

			if (currentChapters.Count() == 0)
			{
				currentChapters.Add(chapter);
			}
			else
			{
				//Could be in the first level, find last chapter in the list
				IChapter beforeChapter = currentChapters.OrderByDescending(c => c.ArabicChapterMark).FirstOrDefault();
				//They are the same level if number of splitted marks are the same, same numeral style
				if (chapter.SplittedOriginalChapterMark.Count() == beforeChapter.SplittedOriginalChapterMark.Count() &&
					(convertNumerals.IsRoman(chapter.SplittedOriginalChapterMark[0]) == convertNumerals.IsRoman(beforeChapter.SplittedOriginalChapterMark[0])))
				{
					currentChapters.Add(chapter);
				}
				else
				{
					//It must be subchapter, add to last one
					beforeChapter.AddSubchapterChapterToBook(chapter);
				}
			}
		}

		private void SetArabicChapterMark()
		{
			NumeralsConverter convertNumerals = new NumeralsConverter();
			if (convertNumerals.IsRoman(_originalChapterMark))
			{
				foreach (string item in _originalChapterMark.Split(new char[] {'.'}, StringSplitOptions.RemoveEmptyEntries))
				{
					if (!string.IsNullOrWhiteSpace(ArabicChapterMark))
					{
						ArabicChapterMark += ".";
					}
					else
					{
						ArabicChapterMark += convertNumerals.RomanToArabic(item).ToString() + ".";
					}
				}
			}
			else
			{
				ArabicChapterMark = _originalChapterMark;
			}
		}
	}
}
