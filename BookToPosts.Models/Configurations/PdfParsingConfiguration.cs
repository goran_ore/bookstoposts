﻿using BookToPosts.Models.Common.Configurations;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookToPosts.Models.Configurations
{
	public class PdfParsingConfiguration : IParsingConfiguration
	{
		public int Id { get; set; }
		public string[] ParagraphDelimitersRegexPattern { get; set; }
		public int? BookChaptersPage { get; set; }
		public string FilePath { get; set; }
		public int LastParagraphEndPage { get; set; }
	}
}
