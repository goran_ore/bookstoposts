﻿using BookToPosts.DAL;
using BookToPosts.FileParser.Common.Parsers;
using BookToPosts.FileParser.Parsers;
using BookToPosts.Models.Common.Configurations;
using BookToPosts.Models.Common.Models;
using BookToPosts.Models.Configurations;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using WordPressSharp;
using WordPressSharp.Constants;
using WordPressSharp.Models;
using XmlRpc.Types;

namespace BookToPosts.V2
{
	class Program
	{
		static void Main(string[] args)
		{
			string path = "";
			int booksChaptersPage = 0;
			int lastParagraphEndPage = -1;
			ConsoleKey response;

			//Get file path from user
			Console.WriteLine("Enter books path:");
			path = Console.ReadLine();
			while (!File.Exists(path) || Path.GetExtension(path) != ".pdf")
			{
				Console.WriteLine("The file you specified (" + path + ") does not exist or is invalid. Please enter correct file path:");
				path = Console.ReadLine();
			}

			//Get book chapters page start
			Console.WriteLine("Enter book chapter page (default 0):");
			var chaptersPageConsole = Console.ReadLine();
			int.TryParse(chaptersPageConsole, out booksChaptersPage);

			//Get last paragraph page
			Console.WriteLine("Enter last paragraph end page (default -1):");
			var lastParagraphPageConsole = Console.ReadLine();
			int.TryParse(lastParagraphPageConsole, out lastParagraphEndPage);


			IParsingConfiguration config = new PdfParsingConfiguration()
			{
				FilePath = path,
				BookChaptersPage = booksChaptersPage,
				ParagraphDelimitersRegexPattern = new string[] { @"(\n\s?\d+\s?\.\s?\n)|(^\d+\s?\.\s?\n)" }, //TODO: REWORK d - digits, rastaviti po brojevima i razmacima
				Id = 0,
				LastParagraphEndPage = lastParagraphEndPage
			};
			IParser parserContent = new PdfParser(config);
			parserContent.Parse();
			parserContent.Book.Title = "Pedijatar U Kući";

			//Show results and ask to initiate import
			Console.WriteLine("Book '" + parserContent.Book.Title + "' has been parsed");
			foreach (IChapter chapter in parserContent.Book.Chapters)
			{
				Console.WriteLine(chapter.Title);
				if (chapter.SubChapters != null)
				{
					foreach (IChapter subChapter in chapter.SubChapters)
					{
						Console.WriteLine("-" + subChapter.Title + "(" + ((IList<IParagraph>)subChapter.BeforeParagraphs).Count.ToString() + ")");
					}
				}				
			}

			Console.WriteLine("Initiate import to default WP database? [Y/N]");
			response = Console.ReadKey(false).Key;
			if (response == ConsoleKey.Y)
			{
				BookToWordpressPostsAdapter wpAdapter = new BookToWordpressPostsAdapter(parserContent.Book);
				//wpAdapter.ImportBookToDatabase();
			}
		}
	}
}
