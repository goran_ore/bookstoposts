﻿namespace BookToPosts.Models.Common.Configurations
{
	public interface IParsingConfiguration
    {
		/// <summary>
		/// Db Id
		/// </summary>
		int Id { get; set; }

		/// <summary>
		/// Collectio of strings of all paragraph delimiters.
		/// </summary>
		string[] ParagraphDelimitersRegexPattern { get; set; }

		/// <summary>
		/// Page number where book chapters are listed.
		/// </summary>
		int? BookChaptersPage { get; set; }

		/// <summary>
		/// Full path to file
		/// </summary>
		string FilePath { get; set; }
		int LastParagraphEndPage { get; set; }
	}
}
