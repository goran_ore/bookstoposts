﻿using System.Collections.Generic;

namespace BookToPosts.Models.Common.Models
{
	public interface IParagraph
    {
		/// <summary>
		/// Db Id
		/// </summary>
		int Id { get; set; }

		/// <summary>
		/// Page number in a book.
		/// </summary>
		int Page { get; set; }

		/// <summary>
		/// Ordinal number in a collection
		/// </summary>
		int OrdinalNumber { get; set; }

		/// <summary>
		/// Separator mark marks the beginning of paragraph in the book.
		/// </summary>
		string SeparatorMark { get; set; }

		/// <summary>
		/// Entire paragraph text.
		/// </summary>
		string FullText { get; set; }

		/// <summary>
		/// Collection of pictures in a paragraph.
		/// </summary>
		IEnumerable<IPicture> Pictures { get; set; }

		/// <summary>
		/// Parent chapter title or paragraph title/mark if title regex was present TOTO: Implement regex pattern for paragraph title recognition
		/// </summary>
		string Title { get; set; }
    }
}
