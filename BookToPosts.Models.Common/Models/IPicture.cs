﻿namespace BookToPosts.Models.Common.Models
{
	public interface IPicture
    {
		/// <summary>
		/// Db Id
		/// </summary>
		int Id { get; set; }

		/// <summary>
		/// Picture ordinal number in a collection
		/// </summary>
		int OrdinalNumber { get; set; }

		/// <summary>
		/// Text string before picture (used for picture location in text).
		/// </summary>
		string BeginningText { get; set; }

		/// <summary>
		/// Trailing text after picture (used for location in text).
		/// </summary>
		string EndingText { get; set; }

		/// <summary>
		/// Text describing picture
		/// </summary>
		string Text { get; set; }

		/// <summary>
		/// Picture location in number of character in text.
		/// </summary>
		int CharacterLocation { get; set; }

		/// <summary>
		/// Picture full file path
		/// </summary>
		string FileLocation { get; set; }

		/// <summary>
		/// Picture data
		/// </summary>
		object Picture { get; set; }
	}
}
