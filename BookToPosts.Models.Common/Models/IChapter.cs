﻿using System.Collections.Generic;

namespace BookToPosts.Models.Common.Models
{
	public interface IChapter
    {
		/// <summary>
		/// Db Id
		/// </summary>
		int Id { get; set; }

		/// <summary>
		/// Ordinal number in a collection (meaning chapter order number)
		/// </summary>
		int OrdinalNumber { get; set; }

		/// <summary>
		/// Either chapter number as number or other characters as chapter number.
		/// </summary>
		string OriginalChapterMark { get; set; }

		/// <summary>
		/// Converted OriginalChapterMark to string of arabic numericals.
		/// </summary>
		string ArabicChapterMark { get; set; }

		/// <summary>
		/// Chapter title (without number).
		/// </summary>
		string Title { get; set; }

		/// <summary>
		/// Text at the beginnig of a chaper or entire text if there are no sub chapters.
		/// </summary>
		IEnumerable<IParagraph> BeforeParagraphs { get; set; }

		/// <summary>
		/// Collection of sub chapters
		/// </summary>
		IEnumerable<IChapter> SubChapters { get; set; }

		/// <summary>
		/// Text at the end of a chapter (may include conclusions)
		/// </summary>
		IEnumerable<IParagraph> AfterParagraphs { get; set; }

		/// <summary>
		/// Splitted arabic chapter mark by '.'
		/// </summary>
		string[] SplittedOriginalChapterMark { get; }

		/// <summary>
		/// Add to appropriate collection by chapter numbering (Top chapters are I, II, III... subchapters I.II., II.I. ...)
		/// Roman numerials will also have priority over arabian ones (I. II. III... over 1. 2. 3. ...)
		/// Returns true if successfull.
		/// </summary>
		/// <param name="chapter"></param>
		/// <returns></returns>
		void AddSubchapterChapterToBook(IChapter chapter);

		/// <summary>
		/// Generates full text from all chapter elements.
		/// </summary>
		/// <returns></returns>
		string FullText();
	}
}
