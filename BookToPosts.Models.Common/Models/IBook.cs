﻿using BookToPosts.Models.Common.Configurations;
using System.Collections.Generic;

namespace BookToPosts.Models.Common.Models
{
	public interface IBook
    {
		/// <summary>
		/// Db Id
		/// </summary>
		int Id { get; set; }

		/// <summary>
		/// Book title
		/// </summary>
		string Title { get; set; }

		/// <summary>
		/// Text at the beginnig of a book (may include content page).
		/// </summary>
		IEnumerable<IParagraph> BeforeParagraphs { get; set; }

		/// <summary>
		/// Collection of chapters
		/// </summary>
		IEnumerable<IChapter> Chapters { get; set; }

		/// <summary>
		/// Text at the end of a book (may include indexes)
		/// </summary>
		IEnumerable<IParagraph> AfterParagraphs { get; set; }

		/// <summary>
		/// Authors
		/// </summary>
		IEnumerable<IAuthor> Authors { get; set; }

		/// <summary>
		/// Generates full text from book elements.
		/// </summary>
		/// <returns></returns>
		string FullText();

		/// <summary>
		/// Book parsing configuration. Used for parsing and constructing original full text.
		/// </summary>
		IParsingConfiguration BookParsingConfiguration { get; set; }

		/// <summary>
		/// Add to appropriate collection by chapter numbering (Top chapters are I, II, III... subchapters I.II., II.I. ...)
		/// Roman numerials will also have priority over arabian ones (I. II. III... over 1. 2. 3. ...)
		/// </summary>
		/// <param name="chapter"></param>
		void AddChapterToBook(IChapter chapter);
	}
}
