﻿using System.Collections.Generic;

namespace BookToPosts.Models.Common.Models
{
	public interface IAuthor
    {
		/// <summary>
		/// Db id
		/// </summary>
		int Id { get; set; }

		/// <summary>
		/// Ordinal number in a collection
		/// </summary>
		int OrdinalNumber { get; set; }

		/// <summary>
		/// Title of the author (Dr, mr, Ms, Mr...)
		/// </summary>
		string Title { get; set; }

		/// <summary>
		/// First name TODO: may include full name
		/// </summary>
		string FirstName { get; set; }

		/// <summary>
		/// Last name
		/// </summary>
		string LastName { get; set; }

		/// <summary>
		/// Collection of paragraphs containing text of authors byographical data.
		/// </summary>
		IEnumerable<IParagraph> Biography { get; set; }
	}
}
