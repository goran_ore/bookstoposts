﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookToPosts.Helpers
{
	public class WordCorrector
	{
		private Dictionary<string, string> _wordReplacements;

		public WordCorrector(Dictionary<string, string> wordReplacements)
		{
			_wordReplacements = wordReplacements;
			if (_wordReplacements == null)
			{
				_wordReplacements = new Dictionary<string, string>();
			}			
		}

		public Dictionary<string,string> GetReplacmentWords()
		{
			return _wordReplacements;
		}

		public string GetWordChangedString(string inputString)
		{
			if (_wordReplacements != null && _wordReplacements.Any())
			{
				foreach (KeyValuePair<string,string> changingWords in _wordReplacements)
				{
					inputString = inputString.Replace(changingWords.Key, changingWords.Value);
				}
			}
			return inputString;
		}
	}
}
