﻿using BookToPosts.Models.Common.Models;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookToPosts.FileParser.Common.Parsers
{
    public interface IParser
    {
		void Parse();
		void SetExternalChapters(IEnumerable<IChapter> chapters, List<IChapter> allOrdered = null);
		void ParseContent();
		void ParseParagraphs();
		IBook Book { get; }
		List<IChapter> GetOrderedChapters();
    }
}
