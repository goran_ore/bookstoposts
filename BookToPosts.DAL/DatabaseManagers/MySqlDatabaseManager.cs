﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookToPosts.DAL.DatabaseManagers
{
	public class MySqlDatabaseManager
	{
		protected string _connectionString;
		protected MySqlConnection _dbConnection;
		protected MySqlTransaction _dbTransaction;

		public MySqlDatabaseManager()
		{
			_connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["wpLittleDot"].ConnectionString;
		}

		public void CreateConnection()
		{
			if (_dbConnection != null)
			{
				throw new Exception("Connection already created");
			}
			if (string.IsNullOrWhiteSpace(_connectionString))
			{
				_connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["wpLittleDot"].ConnectionString;
			}
			_dbConnection = new MySqlConnection(_connectionString);
		}

		public void OpenConnection()
		{
			if (_dbConnection == null)
			{
				this.CreateConnection();
			}
			if (_dbConnection.State != ConnectionState.Open)
			{
				_dbConnection.Open();
			}
		}

		public void CloseConnection()
		{
			if (_dbConnection == null)
			{
				throw new Exception("Database connection does not exist");
			}
			if (this.IsConnection(ConnectionState.Open))
			{
				_dbConnection.Close();
			}
		}

		public bool IsConnection(ConnectionState state)
		{
			if (_dbConnection != null)
			{
				return _dbConnection.State == state;
			}
			return false;
		}

		public MySqlDatabaseManager BeginTransaction()
		{
			if (_dbTransaction == null)
			{
				if (!this.IsConnection(ConnectionState.Open))
				{
					this.OpenConnection();
				}
				_dbTransaction = _dbConnection.BeginTransaction();
			}

			return this;
		}

		public MySqlDatabaseManager CommintTransaction()
		{
			if (_dbTransaction != null)
			{
				_dbTransaction.Commit();
				_dbTransaction = null;
			}
			return this;
		}

		public MySqlDatabaseManager RollbackTransaction()
		{
			if (_dbTransaction != null)
			{
				_dbTransaction.Rollback();
				_dbTransaction = null;
			}
			return this;
		}

		public DataTable Select(string query)
		{
			DataTable result = new DataTable();
			try
			{
				if (!this.IsConnection(ConnectionState.Open))
				{
					this.OpenConnection();
				}
				
				MySqlCommand cmd = new MySqlCommand(query, _dbConnection, _dbTransaction);
				MySqlDataAdapter dap = new MySqlDataAdapter(cmd);
				dap.Fill(result);
			}
			catch (Exception)
			{
				//TODO: Log error
				throw;
			}
			return result;
		}

		public DataTable Select(string query, IList<MySqlParameter> parameters)
		{
			DataTable result = new DataTable();
			try
			{
				if (!this.IsConnection(ConnectionState.Open))
				{
					this.OpenConnection();
				}

				MySqlCommand cmd = new MySqlCommand(query, _dbConnection, _dbTransaction);
				cmd.Parameters.AddRange(parameters.ToArray());
				MySqlDataAdapter dap = new MySqlDataAdapter(cmd);
				dap.Fill(result);
				dap.Dispose();
				cmd.Dispose();
			}
			catch (Exception)
			{
				//TODO: Log error
				throw;
			}
			return result;
		}

		public int InsertRecord(string query)
		{
			int insertedId = -1;
			try
			{
				if (!this.IsConnection(ConnectionState.Open))
				{
					this.OpenConnection();
				}
				MySqlCommand cmd = new MySqlCommand(query, _dbConnection, _dbTransaction);
				insertedId = (int)cmd.ExecuteScalar();
				cmd.Dispose();
			}
			catch (Exception)
			{
				//TODO: Log error
				throw;
			}
			return insertedId;
		}

		public int InsertRecord(string query, IList<MySqlParameter> parameters)
		{
			int insertedId = -1;
			try
			{
				if (!this.IsConnection(ConnectionState.Open))
				{
					this.OpenConnection();
				}
				MySqlCommand cmd = new MySqlCommand(query, _dbConnection, _dbTransaction);
				cmd.Parameters.AddRange(parameters.ToArray());
				insertedId = (int)cmd.ExecuteScalar();
				cmd.Dispose();
			}
			catch (Exception)
			{
				//TODO: Log error
				throw;
			}
			return insertedId;
		}
	}
}
