﻿using BookToPosts.Models.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WordPressSharp;
using WordPressSharp.Constants;
using WordPressSharp.Models;

namespace BookToPosts.DAL
{
	public class BookToWordpressPostsAdapter
	{
		private IBook _book;
		private List<Term> _existingTerms;
		private List<Term> _existingTags;

		private WordPressClient Client
		{
			get
			{
				return new WordPressClient();
			}
		}

		public BookToWordpressPostsAdapter(IBook book)
		{
			_book = book;
			using (Client)
			{
				_existingTerms = Client.GetTerms(TaxonomyType.Category, new TermFilter() { OrderBy = PostOrderBy.Name }).ToList();
				_existingTags = Client.GetTerms(TaxonomyType.Tags, new TermFilter() { OrderBy = PostOrderBy.Name }).ToList();
			}
		}

		public bool ImportBookToDatabase()
		{
			bool result = false;
			if (_book != null)
			{
				List<Post> posts = new List<Post>();

				//Create new category/term
				//TODO: Separate factory method for category -- Term
				Term newBookTerm = new Term()
				{
					Name = _book.Title.Replace("ć", "c"),
					Slug = string.Join("_", _book.Title.Split(' ').Select(t => t)).ToLower().Replace("ć", "c"),
					Taxonomy = TaxonomyType.Category
					//TermGroup and TermTaxonomyId examine what are these
				};
				if (_existingTerms == null || !_existingTerms.Any(t => t.Name == newBookTerm.Name))
				{
					newBookTerm = CreateTerm(newBookTerm);
					_existingTerms.Add(newBookTerm);
				}
				else
				{
					newBookTerm = _existingTerms.Where(t => t.Name == newBookTerm.Name).FirstOrDefault();
				}

				if (!string.IsNullOrWhiteSpace(newBookTerm.Id))
				{

					//Import books paragraphs as posts without category
					//TODO: Separate factory method for Post factory
					if (_book.BeforeParagraphs != null && _book.BeforeParagraphs.Any())
					{
						foreach (IParagraph bookParagraph in _book.BeforeParagraphs)
						{
							posts.Add(GetWpPostFromParagraph(bookParagraph, newBookTerm));
						}
					}
					//Import chapters
					//TODO: Separate factory method for chapter category - Term factory - recursion or something else
					foreach (IChapter chapter in _book.Chapters)
					{
						//TODO: Separate factory method for category -- Term
						Term newChapterTerm = new Term()
						{
							Name = chapter.Title,
							Slug = string.Join("_", chapter.Title.Split(' ').Select(t => t)).ToLower().Replace("ć", "c"),
							Taxonomy = TaxonomyType.Category,
							Parent = newBookTerm.Id
							//TermGroup and TermTaxonomyId examine what are these
						};
						if (_existingTerms == null || !_existingTerms.Any(t => t.Name == newChapterTerm.Name))
						{
							newChapterTerm = CreateTerm(newChapterTerm);
							_existingTerms.Add(newChapterTerm);
						}
						else
						{
							newChapterTerm = _existingTerms.Where(t => t.Name == newChapterTerm.Name).FirstOrDefault();
						}

						if (!string.IsNullOrWhiteSpace(newChapterTerm.Id))
						{
							if (chapter.BeforeParagraphs != null && chapter.BeforeParagraphs.Any())
							{
								foreach (IParagraph chapterParagraph in chapter.BeforeParagraphs)
								{
									posts.Add(GetWpPostFromParagraph(chapterParagraph, newChapterTerm));
								}
							}

							//TODO: Separate factory method insert BeforeParagraphs
							//TODO: Separate factory method insert Subchapters
							if (chapter.SubChapters != null && chapter.SubChapters.Any())
							{
								foreach (IChapter subChapter in chapter.SubChapters)
								{
									//TODO: Separate factory method for category -- Term
									Term newSubChapterTerm = new Term()
									{
										Name = subChapter.Title,
										Slug = string.Join("_", subChapter.Title.Split(' ').Select(t => t)).ToLower().Replace("ć", "c"),
										Taxonomy = TaxonomyType.Category,
										Parent = newChapterTerm.Id
										//TermGroup and TermTaxonomyId examine what are these
									};
									if (_existingTerms == null || !_existingTerms.Any(t => t.Name == newSubChapterTerm.Name))
									{
										newSubChapterTerm = CreateTerm(newSubChapterTerm);
										_existingTerms.Add(newSubChapterTerm);
									}
									else
									{
										newSubChapterTerm = _existingTerms.Where(t => t.Name == newSubChapterTerm.Name).FirstOrDefault();
									}
									if (subChapter.BeforeParagraphs != null && subChapter.BeforeParagraphs.Any())
									{
										foreach (IParagraph subChapterParagraph in subChapter.BeforeParagraphs)
										{
											posts.Add(GetWpPostFromParagraph(subChapterParagraph, newSubChapterTerm));
										}
									}
								}
							}
						}
					}
				}
				if (posts.Any())
				{
					try
					{
						foreach (Post post in posts)
						{
							//TODO: Separate method for client insert category and posts
							using (var client = new WordPressClient(WordPressSiteConfig.Default))
							{
								var id = Convert.ToInt32(client.NewPost(post));
								post.Id = id.ToString();
							}
						}
					}
					catch (Exception)
					{

						throw;
					}
				}

			}
			return result;
		}

		private Term CreateTerm(Term newTerm)
		{
			using (Client)
			{
				string result = Client.NewTerm(newTerm);
				if (!string.IsNullOrWhiteSpace(result))
				{
					int id = -1;
					if (int.TryParse(result, out id))
					{
						if (-1 < id)
						{
							newTerm = Client.GetTerm(newTerm.Taxonomy, id);
						}
					}
				}
			}

			return newTerm;
		}

		private Post CreatePost(Post newPost)
		{
			using (Client)
			{
				string result = Client.NewPost(newPost);
				if (!string.IsNullOrWhiteSpace(result))
				{
					int id = -1;
					if (int.TryParse(result, out id))
					{
						if (-1 < id)
						{
							newPost = Client.GetPost(id);
						}
					}
				}
			}

			return newPost;
		}

		private Post GetWpPostFromParagraph(IParagraph paragraph, Term term = null)
		{
			List<Term> terms = new List<Term>();
			terms.Add(term);
			string paragraphTitle = string.Join(string.Empty, paragraph.Title.Split(' ').Select(t => t.Substring(0, 1).ToUpper())) + " " + paragraph.OrdinalNumber.ToString();

			try
			{
				//TODO: Move this somwhere else
				//Create and assign tags
				Regex rgTags = new Regex(@"(staru|starost|dob|dobi|ima|staro|navršio|navršila|puni|stara|star)\s?(\s|\n)((\d+(\,\d+)?)|jedan|dva|tri|četiri|pet|šest|sedam|osam|devet|deset|jednu|dvije)\s?(\s|\n)(i\spol\s)?(mjeseci|tjedna|tjedana|godinu|godina|godine|mjeseca|dana)");
				MatchCollection results = rgTags.Matches(paragraph.FullText);
				if (results.Count > 0)
				{
					foreach (Match item in results)
					{
						Term newTag = new Term()
						{
							Name = Regex.Replace(item.Value.Replace("staru ", string.Empty).Replace("starost ", string.Empty).Replace("dobi ", string.Empty).Replace("dob ", string.Empty).Replace("ima ", string.Empty).Replace("staro ", string.Empty).Replace("navršio ", string.Empty).Replace("navršila ", string.Empty).Replace("puni ", string.Empty).Replace("stara ", string.Empty).Replace("star ", string.Empty).Trim(), @"\t|\n|\r", " ").Replace("  ", " "), //TODO: Remove this ridiculous ugly hotfix latter
							Slug = string.Join("_", Regex.Replace(item.Value.Replace("staru ", string.Empty).Replace("starost ", string.Empty).Replace("dobi ", string.Empty).Replace("dob ", string.Empty).Replace("ima ", string.Empty).Replace("staro ", string.Empty).Replace("navršio ", string.Empty).Replace("navršila ", string.Empty).Replace("puni ", string.Empty).Replace("stara ", string.Empty).Replace("star ", string.Empty).Trim(), @"\t|\n|\r", " ").Replace("  ", " ").Split(' ').Select(t => t)).ToLower(),
							Taxonomy = TaxonomyType.Tags
						};

						if ((newTag.Name.Contains("mjeseci") || newTag.Name.Contains("tjedna") || newTag.Name.Contains("tjedana") || newTag.Name.Contains("godinu") || newTag.Name.Contains("godina") || newTag.Name.Contains("godine") || newTag.Name.Contains("mjeseca") || newTag.Name.Contains("dana")))
						{
							if (_existingTags == null || !_existingTags.Any(t => t.Name == newTag.Name))
							{
								newTag = CreateTerm(newTag);
								_existingTags.Add(newTag);
							}
							else
							{
								newTag = _existingTags.Where(t => t.Name == newTag.Name).FirstOrDefault();
							}
							terms.Add(newTag);
						}

					}
				}
			}
			catch (Exception)
			{
				
			}
			

			Post newPost = new Post()
			{
				Content = paragraph.FullText,
				Name = paragraph.Title.Replace(" ", "_").ToLower() + paragraph.OrdinalNumber.ToString(),
				PostType = PostType.Post,
				Terms = terms.ToArray(),
				//Title = paragraphTitle,
				Status = PostStatus.Published,
				PublishDateTime = DateTime.UtcNow
			};

			return newPost;
		}
	}
}
