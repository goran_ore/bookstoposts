﻿using BookToPosts.FileParser.Common.Parsers;
using BookToPosts.Models.Common.Configurations;
using BookToPosts.Models.Common.Models;
using BookToPosts.Models.Models;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using BookToPosts.Helpers;

namespace BookToPosts.FileParser.Parsers
{
	public class PdfParser : IParser
	{
		#region fields
		private IParsingConfiguration _parsingConfiguration;
		private KeyValuePair<int, int> contentEndPageLine;
		private List<IChapter> _allChapters;
		private WordCorrector _wordCorrector;
		private List<IParagraph> _unlistedParagraphs;
		#endregion

		#region properties
		public IBook Book { get; private set; }
		#endregion

		#region constructors
		public PdfParser(IParsingConfiguration parsingConfiguration)
		{
			_parsingConfiguration = parsingConfiguration;

			//TODO: Refactor word corrector and include loading form file or memory or database or UI, and add regex expressions
			Dictionary<string, string> wordReplacements = new Dictionary<string, string>();
			wordReplacements.Add("dojen če", "dojenče");
			wordReplacements.Add("Dojen če", "Dojenče");
			wordReplacements.Add("poreme ća", "poremeća");
			wordReplacements.Add("Poreme ća", "Poremeća");
			wordReplacements.Add("te žin", "težin");
			wordReplacements.Add("Te žin", "Težin");
			wordReplacements.Add("gr če", "grče");
			wordReplacements.Add("Gr če", "Grče");
			wordReplacements.Add("ko ž", "kož");
			wordReplacements.Add("Ko ž", "Kož");
			wordReplacements.Add("dojena č", "dojenač");
			wordReplacements.Add("Dojena č", "Dojenač");
			wordReplacements.Add("bi ći", "bići");
			wordReplacements.Add("djevoj č", "djevojč");
			wordReplacements.Add("Djevoj č", "Djevojč");
			wordReplacements.Add("¸  Problem", "Problem");
			wordReplacements.Add("roni č", "ronič");
			_wordCorrector = new WordCorrector(wordReplacements);
		}
		#endregion

		#region methods
		public void Parse()
		{
			Book = new Book();
			PdfReader reader = new PdfReader(_parsingConfiguration.FilePath);
			try
			{
				_parseParagraphs(ref reader);
			}
			catch (Exception)
			{

				//TODO: Log exception
			}
			finally
			{
				reader.Close();
			}
		}

		public void ParseContent()
		{
			PdfReader reader = new PdfReader(_parsingConfiguration.FilePath);
			if (Book == null)
			{
				Book = new Book();
			}
			try
			{
				_parseContent(ref reader);
			}
			catch (Exception)
			{

				//TODO: Log exception
			}
			finally
			{
				reader.Close();
			}
		}

		public void SetExternalChapters(IEnumerable<IChapter> chapters, List<IChapter> allChapters = null)
		{
			if (Book == null)
			{
				Book = new Book();
			}
			Book.Chapters = chapters;
			_allChapters = allChapters;
			contentEndPageLine = new KeyValuePair<int, int>(1, 1);
		}

		/// <summary>
		/// Parse book text. Generates chapters, adds default one if none exist.
		/// </summary>
		/// <param name="reader"></param>
		private void _parseParagraphs(ref PdfReader reader)
		{
			if (Book.Chapters == null || !Book.Chapters.Any())
			{
				_parseContent(ref reader);
				if (!Book.Chapters.Any())
				{
					//Add default chapter
					Book.AddChapterToBook(new Chapter() { OrdinalNumber = 1, OriginalChapterMark = "1.", Title = "Title", ArabicChapterMark = "1." });
					contentEndPageLine = new KeyValuePair<int, int>(0, 0);
				}
			}
			List<IChapter> currentChapters = (List<IChapter>)Book.Chapters;
			List<IParagraph> unlistedParagraphTemp = new List<IParagraph>();
			IChapter nextChapter = currentChapters.OrderBy(c => c.OrdinalNumber).FirstOrDefault();
			IChapter beforeChapter = null;
			IParagraph currentParagraph = new Paragraph();
			StringBuilder text = new StringBuilder();
			int bookPageCounter = contentEndPageLine.Key;
			_unlistedParagraphs = new List<IParagraph>();
			//Go through all chapters, find appropriate one
			foreach (Chapter chapter in _allChapters.Where(c => c.OrdinalNumber >= nextChapter.OrdinalNumber).OrderBy(c => c.OrdinalNumber))
			{
				int chapterPagesCount = 0;
				int lastParagraphSeparatorNumber = 0;
				int lastParagraphEndPage = _parsingConfiguration.LastParagraphEndPage;
				if (-1 <= lastParagraphEndPage)
				{
					lastParagraphEndPage = reader.NumberOfPages;
				}
				bool currentChapterTitleFound = false;
				bool nextChapterOnPagePresent = false;
				chapter.BeforeParagraphs = new List<IParagraph>();
				List<IParagraph> chapterParagraphs = (List<IParagraph>)chapter.BeforeParagraphs;
				for (int page = bookPageCounter; page <= _parsingConfiguration.LastParagraphEndPage; page++)	//TODO: Examine real lenght of the book, inform user when settin it if it's to great
				{
					ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
					string currentText = PdfTextExtractor.GetTextFromPage(reader, page, strategy);
					//currentText = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(currentText)));
					currentText = _wordCorrector.GetWordChangedString(Encoding.UTF8.GetString(Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(currentText))));

					//Regex rTitle = new Regex(@"((\n||(" + chapter.OriginalChapterMark + @"\s)||[""])" + chapter.Title.Replace("(", @"\(").Replace(")", @"\)").Replace(",", @"\,").Replace(" ", @"(\n|\s)") + @"\s?(\n||[""]))");
					Regex rTitle = new Regex(@"\n?\s?" + chapter.Title.Replace("(", @"\(").Replace(")", @"\)").Replace(",", @"\,").Replace(" ", @"\s?\n?") + @"\s?\n");
					//Title found, split text and handle halfs
					if (!currentChapterTitleFound && rTitle.Match(currentText).Success)
					{
						//This holds texts, splited in the middle, first half is from this chapter, second is for current chapter and will be examined in next loop
						//currentText = Regex.Split(currentText, @"((\n||(" + chapter.OriginalChapterMark + @"\s)||[""])" + chapter.Title.Replace("(", @"\(").Replace(")", @"\)").Replace(",", @"\,").Replace(" ", @"(\n|\s)") + @"\s?(\n||[""]))").LastOrDefault();
						currentText = Regex.Split(currentText, @"\n?\s?" + chapter.Title.Replace("(", @"\(").Replace(")", @"\)").Replace(",", @"\,").Replace(" ", @"\s?\n?") + @"\s?\n").LastOrDefault();
						currentChapterTitleFound = true;    //To avoid entering here twice in one chapter
					}

					//Set next chapter
					nextChapter = _allChapters.Any(c => c.OrdinalNumber > chapter.OrdinalNumber) ? _allChapters.Where(c => c.OrdinalNumber > chapter.OrdinalNumber).OrderBy(c => c.OrdinalNumber).FirstOrDefault() : null;
					if (nextChapter != null)
					{
						//Set the next chapter separator (to know where to stop reading, and change chapters)
						//Regex rNextTitleSplitRegex = new Regex(@"((\n||(" + nextChapter.OriginalChapterMark + @"\s)||[""])" + nextChapter.Title.Replace("(", @"\(").Replace(")", @"\)").Replace(",", @"\,").Replace(" ", @"(\n||\s)") + @"\s?(\n||[""]))");  //new Regex(rgPreMark + nextChapter.OriginalChapterMark + rgPostMark + nextChapter.Title.Replace(" ", rgNewLinSpace) + rgNewLinSpace + " ?)");
						Regex rNextTitleSplitRegex = new Regex(@"\n?\s?" + nextChapter.Title.Replace("(", @"\(").Replace(")", @"\)").Replace(",", @"\,").Replace(" ", @"\s?\n?") + @"\s?\n");  //new Regex(rgPreMark + nextChapter.OriginalChapterMark + rgPostMark + nextChapter.Title.Replace(" ", rgNewLinSpace) + rgNewLinSpace + " ?)");

						//Handle text 
						//Check for next chapter, and remove all lines after it
						Match nextTextFound = rNextTitleSplitRegex.Match(currentText);
						if (nextTextFound.Success)
						{
							nextChapterOnPagePresent = true;
							//currentText = Regex.Split(currentText, @"((\n||(" + nextChapter.OriginalChapterMark + @"\s)||[""])" + nextChapter.Title.Replace("(", @"\(").Replace(")", @"\)").Replace(",", @"\,").Replace(" ", @"(\n|\s)") + @"\s?(\n||[""]))").FirstOrDefault(); //TODO: For the love of God and all good things, rework this!
							currentText = Regex.Split(currentText, @"\n?\s?" + nextChapter.Title.Replace("(", @"\(").Replace(")", @"\)").Replace(",", @"\,").Replace(" ", @"\s?\n?") + @"\s?\n").FirstOrDefault(); //TODO: For the love of God and all good things, rework this!
						}
					}


					//Now, split up by paragraphs regex, all of them at once
					string[] allSplitedParagraphs = Regex.Split(currentText, String.Join(")|(", _parsingConfiguration.ParagraphDelimitersRegexPattern));
					if (chapter.OriginalChapterMark == "X." && chapterPagesCount > 0)   //TODO: Implement user options for certain chapters (like this one, this chapter does not have number separated, but with titles and new lines)
					{
						if (!chapter.BeforeParagraphs.Any())
						{
							currentParagraph = new Paragraph() { Page = bookPageCounter, OrdinalNumber = 1, Title = chapter.Title };
							chapterParagraphs.Add(currentParagraph);
						}
						currentParagraph.FullText += Regex.Replace(allSplitedParagraphs[0].Trim(), @"\n?\d+\s\d+\s?\n?", string.Empty).Trim(); // allSplitedParagraphs[0].Trim();
						//Remove page numbers TODO: Move out of here
					}
					else
					{
						//First must always be from "current" or rather "past" paragraph, so fill that in first
						currentParagraph.FullText += Regex.Replace(allSplitedParagraphs[0].Trim(), @"\n?\d+\s\d+\s?\n?", string.Empty).Trim(); // allSplitedParagraphs[0].Trim();
					}


					if (allSplitedParagraphs.Length > 1)
					{
						for (int i = 1; i < allSplitedParagraphs.Length; i++)
						{
							//List contains paragraph separator mark, add that info
							if (!string.IsNullOrWhiteSpace(allSplitedParagraphs[i]) && Regex.IsMatch(allSplitedParagraphs[i], String.Join(")||(", _parsingConfiguration.ParagraphDelimitersRegexPattern)))
							{
								//Check for ordering numbers and reject ones out of order TODO: Either improve regex or add option for paragraphs order and separator mark ordering
								string mark = allSplitedParagraphs[i].Replace("\n", string.Empty).Replace(".", string.Empty);
								int markInt = 0;
								string nextLine = (i + 1) >= allSplitedParagraphs.Count() ? string.Empty : allSplitedParagraphs[i + 1];
								if (int.TryParse(mark, out markInt))
								{
									if ((lastParagraphSeparatorNumber > 0 && lastParagraphSeparatorNumber <= markInt) || (lastParagraphSeparatorNumber == 0 && markInt == 1))
									{
										currentParagraph = new Paragraph() { Page = bookPageCounter, OrdinalNumber = markInt, Title = chapter.Title };
										currentParagraph.SeparatorMark = allSplitedParagraphs[i];
										chapterParagraphs.Add(currentParagraph);
										lastParagraphSeparatorNumber = markInt;
									}
									else if (beforeChapter != null)
									{
										//Check this paragraph belongs to previous chapter by ordinal numbering
										if (beforeChapter.BeforeParagraphs.Any() && beforeChapter.BeforeParagraphs.OrderBy(p => p.OrdinalNumber).LastOrDefault().OrdinalNumber == (markInt - 1))
										{
											currentParagraph = new Paragraph() { Page = bookPageCounter, OrdinalNumber = markInt, Title = chapter.Title };
											currentParagraph.SeparatorMark = allSplitedParagraphs[i];
											((List<IParagraph>)beforeChapter.BeforeParagraphs).Add(currentParagraph);
										}
										else if (!chapter.BeforeParagraphs.Any(p => p.OrdinalNumber == markInt && p.FullText == nextLine)
											&& !beforeChapter.BeforeParagraphs.Any(p => p.OrdinalNumber == markInt && p.FullText == nextLine))
										{
											_unlistedParagraphs.Add(new Paragraph() { OrdinalNumber = markInt, Page = bookPageCounter, FullText = nextLine, Title = chapter.Title });
										}
									}
									else
									{
										//Put unlisted to temp list for latter joining with the right chapter
										_unlistedParagraphs.Add(new Paragraph() { OrdinalNumber = markInt, Page = bookPageCounter, FullText = nextLine, Title = chapter.Title });
									}
								}

							}
							else
							{
								currentParagraph.FullText += " " + Regex.Replace(allSplitedParagraphs[i].Trim(), @"\n?\d+\s\d+\s?\n?", string.Empty).Trim(); // allSplitedParagraphs[i].Trim();
							}
						}
					}

					if (nextChapterOnPagePresent)
					{
						if (_unlistedParagraphs.Any())
						{
							AddUnlistedParagraphsToChapter(beforeChapter, true);
							AddUnlistedParagraphsToChapter(chapter);
							if (!chapter.BeforeParagraphs.Any())
							{
								//Means that current chapters are in chapter before, replace them here
								chapter.BeforeParagraphs = beforeChapter.BeforeParagraphs;
								beforeChapter.BeforeParagraphs = null;
							}
						}

						beforeChapter = chapter;
						break;
					}
					bookPageCounter++;
					chapterPagesCount++;
				}
			}
		}

		private void AddUnlistedParagraphsToChapter(IChapter chapter, bool isPreviousChapter = false)
		{
			List<int> addedParagraphs = new List<int>();
			List<int> unlistedOrdinals = _unlistedParagraphs.OrderBy(p => p.OrdinalNumber).Select(p => p.OrdinalNumber).ToList();
			foreach (IParagraph unlisted in _unlistedParagraphs.OrderBy(p => p.OrdinalNumber))
			{
				List<int> currentChapterNumbers = chapter.BeforeParagraphs.OrderBy(p => p.OrdinalNumber).Select(p => p.OrdinalNumber).Distinct().ToList();

				if (currentChapterNumbers.Any())
				{
					for (int n = 0; n < currentChapterNumbers.Count(); n++)
					{
						if ((n + 1) < currentChapterNumbers.Count())
						{
							int nextOrdinal = currentChapterNumbers[n + 1];
							if (currentChapterNumbers[n] < unlisted.OrdinalNumber && (nextOrdinal >= unlisted.OrdinalNumber && !isPreviousChapter))// || (isPreviousChapter && nextOrdinal > unlisted.OrdinalNumber)))
							{
								((List<IParagraph>)chapter.BeforeParagraphs).Add(unlisted);
								addedParagraphs.Add(_unlistedParagraphs.IndexOf(unlisted));
								break;
							}
						}
						else if (currentChapterNumbers[n] <= unlisted.OrdinalNumber)
						{
							((List<IParagraph>)chapter.BeforeParagraphs).Add(unlisted);
							addedParagraphs.Add(_unlistedParagraphs.IndexOf(unlisted));
							break;
						}
					}
				}
				else if (unlisted.OrdinalNumber == 1)
				{
					((List<IParagraph>)chapter.BeforeParagraphs).Add(unlisted);
					addedParagraphs.Add(_unlistedParagraphs.IndexOf(unlisted));
				}
				else if (chapter.Title == "Kronično bolesno dijete")	//TODO: Remove this extreme hotfix!!!
				{
					((List<IParagraph>)chapter.BeforeParagraphs).Add(unlisted);
					addedParagraphs.Add(_unlistedParagraphs.IndexOf(unlisted));
				}
			}


			//Remove added paragraphs
			if (addedParagraphs.Any() && addedParagraphs.Count() <= _unlistedParagraphs.Count())
			{
				foreach (int removeIndex in addedParagraphs.OrderByDescending(z => z))
				{
					_unlistedParagraphs.RemoveAt(removeIndex);
				}
			}
		}


		/// <summary>
		/// Parse content pages and generates chapter collection.
		/// </summary>
		/// <param name="reader"></param>
		private void _parseContent(ref PdfReader reader)
		{
			if (_parsingConfiguration.BookChaptersPage.HasValue)
			{
				Book.Chapters = new List<IChapter>();
				List<IChapter> currentChapters = (List<IChapter>)Book.Chapters;
				_allChapters = new List<IChapter>();

				string arabianNumeral = @"(?m)^\¸?\s?\d+\.?(?:\.\d+)*[ \t]+\S.*$";  //TODO: Added \¸?\s? because of specific error in book print Add support for misspeling in text
				string arabianNumeralsExtract = @"\d+\.?";
				string romanNumerals = @"(?m)^\¸?\s?[MDCLXVI]+\.?(?:\.[MDCLXVI]+?\.?)*[ \t]+\S.*$"; //TODO: Added \¸?\s? because of specific error in book print Add support for misspeling in text
				string romanNumeralsExtract = @"[MDCLXVI]+\.?\s";
				Regex r = new Regex(arabianNumeral, RegexOptions.IgnoreCase);
				Regex rExtract = new Regex(arabianNumeralsExtract, RegexOptions.CultureInvariant);
				Regex rRoman = new Regex(romanNumerals, RegexOptions.CultureInvariant);
				Regex rRomanExtract = new Regex(romanNumeralsExtract, RegexOptions.CultureInvariant);
				string firstTitle = string.Empty;
				bool breakCycle = false;

				//Read content at it's beginning, while there is content
				for (int page = _parsingConfiguration.BookChaptersPage.Value; page < reader.NumberOfPages; page++)
				{
					ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
					string currentText = PdfTextExtractor.GetTextFromPage(reader, page, strategy);
					currentText = _wordCorrector.GetWordChangedString(Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(currentText))));

					string[] lines = currentText.Split('\n');
					int lineNumber = 1;
					string lastLine = string.Empty;

					foreach (string line in lines)
					{
						if (!string.IsNullOrWhiteSpace(line))
						{
							Match mRoman = rRoman.Match(line);
							Match mArabic = r.Match(line);

							if (mRoman.Success || mArabic.Success)
							{
								IChapter chapter = new Chapter() { OrdinalNumber = _allChapters.Count };

								Match mRomanChapterNumber = rRomanExtract.Match(line);
								Match mArabicChapterNumber = rExtract.Match(line);

								//Extract chapter number(s)
								if (mRomanChapterNumber.Success)
								{
									chapter.OriginalChapterMark = mRomanChapterNumber.Value.Trim();
									//Extract name (chapter name). TODO: Add option for parsing (not removing) page number in a book
									chapter.Title = line.Replace(mRomanChapterNumber.Value.Trim(), string.Empty).Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(); //TODO: Add support for misspeling in text
								}
								else if (mArabicChapterNumber.Success)
								{
									chapter.OriginalChapterMark = mArabicChapterNumber.Value;
									//Extract name (chapter name). TODO: Add option for parsing (not removing) page number in a book
									chapter.Title = _wordCorrector.GetWordChangedString(line.Replace(mArabicChapterNumber.Value, string.Empty).Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries)[0].Trim());
								}
								else
								{
									throw new Exception("PdfParser.ParseContent ERROR: No matching numerals when expected. Line number: " + lineNumber.ToString() + "; Line content: " + line);
								}

								if (string.IsNullOrWhiteSpace(firstTitle))
								{
									firstTitle = chapter.Title;
								}

								//Add to appropriate collection
								Book.AddChapterToBook(chapter);
								//Add to list of all chapters (flattened list) TODO: Put this into linq expression only when needed
								_allChapters.Add(chapter);

								//Advance line number only if ther is a match
								lineNumber++;
							}

							//Regex rg = new Regex(@"\b" + firstTitle.Replace(" ", @"\b(\n|\s)\b") + @"\b\s?(\n||[""])");

							if (!string.IsNullOrWhiteSpace(firstTitle) && _allChapters.Count > 2 && Regex.IsMatch((lastLine.Trim() + " " + line.Trim()).Trim(), @"" + firstTitle.Replace(" ", @"(\n|\s)") + @"\s?(\n||[""])"))
							{
								breakCycle = true;
								contentEndPageLine = new KeyValuePair<int, int>(page, lineNumber);
								break;
							}
							//Break loop if conditions are met (recognized end of content)
							if (!string.IsNullOrWhiteSpace(firstTitle) && (firstTitle == line.Trim() || (lastLine + " " + line.Trim()).Trim() == firstTitle || page > 30)) //TODO: Rework break for title match, add n line support
							{
								breakCycle = true;
								contentEndPageLine = new KeyValuePair<int, int>(page, lineNumber);
								break;
							}


							lastLine = line;
						}
					}

					if (breakCycle)
					{
						break;
					}
				}
			}
		}

		public List<IChapter> GetOrderedChapters()
		{
			return _allChapters;
		}

		public void ParseParagraphs()
		{
			PdfReader reader = new PdfReader(_parsingConfiguration.FilePath);
			try
			{
				_parseParagraphs(ref reader);
			}
			catch (Exception ex)
			{

				//TODO: Log exception
			}
			finally
			{
				reader.Close();
			}
		}

		#endregion
	}
}
